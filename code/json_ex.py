import argparse
import pandas as pd


# Configura l'argomento della linea di comando
parser = argparse.ArgumentParser(description="Parsa un csv e stampa i peggiori 10 seller")
parser.add_argument('file_path', type=str, help='Il percorso del file CSV da caricare')
args = parser.parse_args()

# Percorso del file CSV
file_path = args.file_path

# Carica il file CSV utilizzando pandas
try:
    df = pd.read_csv(file_path)
    print("File CSV caricato con successo.")
    sorted_df = df.sort_values(by='avg_review', ascending=True)
    # Seleziona i primi 10 record
    worst_10_sellers = sorted_df.head(10) 
    # Seleziona solo le colonne 'seller_name' e 'avg_review'
    worst_10_sellers = worst_10_sellers[['seller_name', 'avg_review']]
    print(worst_10_sellers.to_json(orient='records', indent=4))
except FileNotFoundError:
    print(f"Il file specificato non è stato trovato: {file_path}")
except pd.errors.EmptyDataError:
    print("Il file CSV è vuoto.")
except pd.errors.ParserError:
    print("Errore durante il parsing del file CSV.")
except Exception as e:
    print(f"Si è verificato un errore: {e}")


#[
#    {
#        "seller_name": "XXX",
#        "avg_review": x.y
#    },
#        {
#        "seller_name": "XXX",
#        "avg_review": x.y
#    }
#]