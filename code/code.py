import os
import sys
import pandas as pd

# Leggi il percorso del file dalla variabile d'ambiente
file_path = os.getenv('DATA_SOURCE_PATH')

if file_path is None:
    raise ValueError("La variabile d'ambiente DATA_SOURCE_PATH non è stata impostata.")

# Carica il file CSV utilizzando pandas
try:
    df = pd.read_csv(file_path)
    print("File CSV caricato con successo.")
    print(df.head())  # Stampa le prime righe del DataFrame
except FileNotFoundError:
    print(f"Il file specificato non è stato trovato: {file_path}")
    sys.exit(-3)
except pd.errors.EmptyDataError:
    print("Il file CSV è vuoto.")
except pd.errors.ParserError:
    print("Errore durante il parsing del file CSV.")
except Exception as e:
    print(f"Si è verificato un errore: {e}")



print("---------------------")
print("this is the first excercise")
data = [['Cloud', 18], ['Big Data', 22], ['Fondamenti Informatica', 27]]
#df = pd.DataFrame(data, columns=['Esame', 'voto'])
print(df.describe())
print("---------------------")
